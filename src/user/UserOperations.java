package user;

import java.math.BigInteger;
import javax.jws.WebService;

@WebService(targetNamespace = "http://socialMediaServer.com/wsdl")
public interface UserOperations {

	String loginUser(String SQL_Query,String Parameters,String correlationId);
	
	String createUser(String SQL_Query,String Parameters,String correlationId);
	
	String getNewsFeed(String SQL_Query,String Parameters,String correlationId);
	
	String postStatusUpdate(String SQL_Query,String Parameters,String correlationId);

	String loadFriendList(String SQL_Query, String Parameters, String correlationId);

	String sendFiendRequest(String SQL_Query, String Parameters, String correlationId);

	String renderFriendListPage(String SQL_Query, String Parameters, String correlationId);

	String loadMyFriendList(String SQL_Query, String Parameters, String correlationId);

	String loadPendingFriendList(String SQL_Query, String Parameters, String correlationId);

	String rejectFriendRequest(String SQL_Query, String Parameters, String correlationId);

	String acceptFriendRequest(String SQL_Query, String Parameters, String correlationId);

	String renderUserDetailsPage(String SQL_Query, String Parameters, String correlationId);

	String getLifeEvents(String SQL_Query, String Parameters, String correlationId);

	String updateProfilePicture(String SQL_Query, String Parameters, String correlationId);

	String renderGroupsPage(String SQL_Query, String Parameters, String correlationId);

	String loadAllGroups(String SQL_Query, String Parameters, String correlationId);

	String loadMyGroups(String SQL_Query, String Parameters, String correlationId);

	String addUserToGroup(String SQL_Query, String Parameters, String correlationId);

	String removeUserFromGroup(String SQL_Query, String Parameters, String correlationId);

	String createGroup(String SQL_Query, String Parameters, String correlationId);

	String unFriendUserRequest(String SQL_Query, String Parameters, String correlationId);

	String navToGroupDetailPage(String SQL_Query, String Parameters, String correlationId);

	String getGroupDetails(String SQL_Query, String Parameters, String correlationId);

	String getGroupUserList(String SQL_Query, String Parameters, String correlationId);

	String getGroupNonMembers(String SQL_Query, String Parameters, String correlationId);

	String addUserToGroupAdmin(String SQL_Query, String Parameters, String correlationId);

	String removeUserFromGroupAdmin(String SQL_Query, String Parameters, String correlationId);

	String deleteGroup(String SQL_Query, String Parameters, String correlationId);

	String navToFriendDetailPage(String SQL_Query, String Parameters, String correlationId);

	String getFriendDetails(String SQL_Query, String Parameters, String correlationId);
	
}
