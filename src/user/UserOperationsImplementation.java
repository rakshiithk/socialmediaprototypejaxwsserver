package user;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@Stateless
@WebService(
		portName = "UserOperationsPort",
		serviceName = "UserOperation",
		targetNamespace = "http://socialMediaPrototypeServer.com/wsdl",
		endpointInterface = "user.UserOperations")
public class UserOperationsImplementation implements UserOperations{

	//JDBC driver name and database URL
	static final private String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final private String DB_URL = "jdbc:mysql://localhost/SocialMediaPrototypeDB?allowMultiQueries=true";

	//Database credentials
	static final private String USER = "root";
	static final private String PASS = "root";
	
	private boolean connectionPooling = false;
	
	@Resource(name="jdbc/SocialMediaPrototypeDB")
	private DataSource ds;

	@Override
	public String loginUser(String SQL_Query,String Parameters,String correlationId) {
		String results = "";
		String username = "";
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			System.out.println("loginUser Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				username = (String) requestJSONParam.get("username");
				
				//Validating if all the fields are sent by the client and only proceed if the request is valid
				if(username != null && username != "") {
					
					//Query Executed with Connection pooling of MYSQL Connections
					if(connectionPooling) {
						connection = ds.getConnection();
					}else {
						Class.forName("com.mysql.jdbc.Driver");
						connection = DriverManager.getConnection(DB_URL,USER,PASS);
					}
					
					statement = connection.prepareStatement(SQL_Query);
					statement.setString(1, (String) requestJSONParam.get("username"));
					result = statement.executeQuery();
					System.out.println("Statement Executed !! ==> " + result.getStatement());
					results = processResults(result,correlationId);
					System.out.print("loginUser Result in Json Format ===> " + results);	
				}else {
					results = createBlankJSONResponse(correlationId);
				}
			}else {
				results = createBlankJSONResponse(correlationId);
			}	
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		//Respond to the user request
		System.out.print("loginUser Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String createUser(String SQL_Query,String Parameters,String correlationId) {
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		String FIRST_NAME = "",
		LAST_NAME = "",
		EMAIL_ADDR = "",
		DATE_OF_BIRTH = "",
		PASSWORD = "",
		GENDER = "",
		IMAGE_URL = "";
	
		try {
			System.out.println("createUser Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters); 
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				FIRST_NAME = (String) requestJSONParam.get("FIRST_NAME");
				LAST_NAME = (String) requestJSONParam.get("LAST_NAME");
				EMAIL_ADDR = (String) requestJSONParam.get("EMAIL_ADDR");
				DATE_OF_BIRTH = (String) requestJSONParam.get("DATE_OF_BIRTH");
				PASSWORD = (String) requestJSONParam.get("PASSWORD");
				GENDER = (String) requestJSONParam.get("GENDER");
				IMAGE_URL = (String) requestJSONParam.get("IMAGE_URL");
				
				//Validating wether the required fields are sent by the client in the WS call and proceed only if request is valid
				if((FIRST_NAME != null && FIRST_NAME != "") 
						&& (LAST_NAME != null && LAST_NAME != "") 
							&& (EMAIL_ADDR != null && EMAIL_ADDR != "")
								&& (DATE_OF_BIRTH != null && DATE_OF_BIRTH != "")
									&& (PASSWORD != null && PASSWORD != "")
										&& (GENDER != null && GENDER != "")
											&& (IMAGE_URL != null && IMAGE_URL != "")) {
					
					//Query Executed with Connection pooling of MYSQL Connections
					if(connectionPooling) {
						connection = ds.getConnection();
					}else {
						Class.forName("com.mysql.jdbc.Driver");
						connection = DriverManager.getConnection(DB_URL,USER,PASS);
					}
					statement = connection.prepareStatement(SQL_Query);
					statement.setString(1,FIRST_NAME);
					statement.setString(2,LAST_NAME);
					statement.setString(3,EMAIL_ADDR);
					statement.setString(4,DATE_OF_BIRTH);
					statement.setString(5,PASSWORD);
					statement.setString(6,GENDER);
					statement.setString(7,IMAGE_URL);
					
					int noOfRowsAffected = statement.executeUpdate();
					System.out.println("noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
					
					obj = new JSONObject();
					list = new JSONArray();
					
					if(noOfRowsAffected > 0) {
						obj.put("code", 200);
						result = statement.getGeneratedKeys();
						if (result.next()){
							obj.put("ROW_ID", result.getInt(1));
						}
					}else {
						obj.put("code", 404);
						obj.put("err", "Error In Creating User!! Please try again...");
					}
					list.add(obj);
					
					//Passing request CorrelationId Back to response
					obj = AddResponseHeaders(list,correlationId);
					
					//Converting JSON to String for Sending the response.
					results = ConvertJSONToString(obj);				
				}else {
					//Send Error Response if Invalid Request is sent with missing information.
					results = createBlankJSONResponse(correlationId);
				}
			}else {
				//Send Error Response if Invalid Request is sent with missing information.
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("createUser Result in Json Format ===> " + results);
		return results;
	}

	@Override
	public String getNewsFeed(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
			
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String postStatusUpdate(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("postStatusUpdate Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				//Convert Request to JSON Object
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
							
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("USER_ID"));
				statement.setString(2, (String) requestJSONParam.get("POST_MESSAGE"));
				statement.setString(3, (String) requestJSONParam.get("IMAGE_URL"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("postStatusUpdate noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				
			}	
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("postStatusUpdate Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String loadFriendList(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("loadFriendList Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String sendFiendRequest(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("sendFiendRequest Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("USER1"));
				statement.setLong(2, (long) requestJSONParam.get("USER2"));
				statement.setString(3, (String) requestJSONParam.get("ACCEPTED"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("sendFiendRequest noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("sendFiendRequest Result in Json Format ===> " + results);	
		return results;
	}

	@Override
	public String renderFriendListPage(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("renderFriendListPage Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResults(result,correlationId);
				
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("renderFriendListPage Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String loadMyFriendList(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("loadMyFriendList Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("loadMyFriendList Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String loadPendingFriendList(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
				System.out.print("Result in Json Format ===> " + results);		
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally{
		}
		return results;
	}
	
	@Override
	public String rejectFriendRequest(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("rejectFriendRequest Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("USER1"));
				statement.setLong(2, (long) requestJSONParam.get("USER2"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("rejectFriendRequest noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("rejectFriendRequest Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String acceptFriendRequest(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("acceptFriendRequest Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("USER1"));
				statement.setLong(2, (long) requestJSONParam.get("USER2"));
				statement.setLong(3, (long) requestJSONParam.get("USER2"));
				statement.setLong(4, (long) requestJSONParam.get("USER1"));
				statement.setString(5, (String) requestJSONParam.get("ACCEPTED"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("acceptFriendRequest noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("acceptFriendRequest Result in Json Format ===> " + results);	
		return results;
	}

	@Override
	public String renderUserDetailsPage(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("renderUserDetailsPage Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResults(result,correlationId);				
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("renderUserDetailsPage Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String getLifeEvents(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String updateProfilePicture(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("updateProfilePicture Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setString(1, (String) requestJSONParam.get("newPath"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("updateProfilePicture noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("updateProfilePicture Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String renderGroupsPage(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("renderGroupsPage Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResults(result,correlationId);	
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		
		System.out.print("renderGroupsPage Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String loadAllGroups(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("loadAllGroups Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("loadAllGroups Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);				
			}else {
				results = createBlankJSONResponse(correlationId);
			}	
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("loadAllGroups Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String loadMyGroups(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("loadMyGroups Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("loadMyGroups Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String addUserToGroup(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("addUserToGroup Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				statement.setLong(2, (long) requestJSONParam.get("USER_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("addUserToGroup noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In addUserToGroup User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("addUserToGroup Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String removeUserFromGroup(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("removeUserFromGroup Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("USER_ID"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("removeUserFromGroup noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("removeUserFromGroup Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String createGroup(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("createGroup Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setString(1, (String) requestJSONParam.get("GROUP_NAME"));
				statement.setString(2, (String) requestJSONParam.get("GROUP_INFO"));
				statement.setString(3, (String) requestJSONParam.get("IMAGE_URL"));
				statement.setLong(4, (long) requestJSONParam.get("CREATED_BY"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("createGroup noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("createGroup Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String unFriendUserRequest(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("unFriendUserRequest Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("friendid"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				statement.setLong(3, (long) requestJSONParam.get("friendid"));
				statement.setLong(4, (long) requestJSONParam.get("ROW_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("unFriendUserRequest sendFiendRequest noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}		
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("unFriendUserRequest Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String navToGroupDetailPage(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("navToGroupDetailPage Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("navToGroupDetailPage Statement Executed !! ==> " + result.getStatement());
				results = processResults(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("navToGroupDetailPage Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String getGroupDetails(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("getGroupDetails Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("groupid"));
				result = statement.executeQuery();
				System.out.println("getGroupDetails Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("getGroupDetails Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String getGroupUserList(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("getGroupDetails Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("groupid"));
				result = statement.executeQuery();
				System.out.println("Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String getGroupNonMembers(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("getGroupNonMembers Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("groupid"));
				result = statement.executeQuery();
				System.out.println("getGroupNonMembers Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("getGroupNonMembers Result in Json Format ===> " + results);	
		return results;
	}

	@Override
	public String addUserToGroupAdmin(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("addUserToGroupAdmin Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("GROUP_ID"));
				statement.setLong(2, (long) requestJSONParam.get("ROW_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("addUserToGroupAdmin noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("addUserToGroupAdmin Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String removeUserFromGroupAdmin(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("removeUserFromGroupAdmin Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				statement.setLong(2, (long) requestJSONParam.get("GROUP_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("removeUserFromGroupAdmin noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("removeUserFromGroupAdmin Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String deleteGroup(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		JSONArray list = null;
		JSONObject obj = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("deleteGroup Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("GROUP_ID"));
				
				int noOfRowsAffected = statement.executeUpdate();
				System.out.println("deleteGroup noOfRowsAffected in CreateUser ==> " + noOfRowsAffected);
				
				obj = new JSONObject();
				list = new JSONArray();
				
				if(noOfRowsAffected > 0) {
					obj.put("code", 200);
					result = statement.getGeneratedKeys();
					if (result.next()){
						obj.put("ROW_ID", result.getInt(1));
					}
				}else {
					obj.put("code", 404);
					obj.put("err", "Error In Creating User!! Please try again...");
				}
				list.add(obj);
				
				//Passing request CorrelationId Back to response
				obj = AddResponseHeaders(list,correlationId);
				if(noOfRowsAffected > 0)
					obj.put("code", 200);
				
				//Converting JSON to String for Sending the response.
				results = ConvertJSONToString(obj);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("deleteGroup Result in Json Format ===> " + results);	
		return results;
	}
	
	@Override
	public String navToFriendDetailPage(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		System.out.println("navToFriendDetailPage Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("ROW_ID"));
				result = statement.executeQuery();
				System.out.println("navToFriendDetailPage Statement Executed !! ==> " + result.getStatement());
				results = processResults(result,correlationId);
			}else {
				results = createBlankJSONResponse(correlationId);
			}
		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("navToFriendDetailPage Result in Json Format ===> " + results);
		return results;
	}
	
	@Override
	public String getFriendDetails(String SQL_Query,String Parameters,String correlationId) {
		String results = null;
		ResultSet result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		
		System.out.println("getFriendDetails Request ==> SQL_Query ==> " + SQL_Query + " and Parameters ==> " + Parameters);
		try {
			
			//Defensive coding to check if the Request has valid parameters passed
			if(SQL_Query !=  null && SQL_Query != "" && Parameters != null && Parameters != "" && correlationId != null && correlationId != "") {
				Object requestParam =JSONValue.parse(Parameters);
				JSONObject requestJSONParam = (JSONObject) requestParam;
				
				//Query Executed with Connection pooling of MYSQL Connections
				if(connectionPooling) {
					connection = ds.getConnection();
				}else {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection(DB_URL,USER,PASS);
				}
				statement = connection.prepareStatement(SQL_Query);
				statement.setLong(1, (long) requestJSONParam.get("friendid"));
				result = statement.executeQuery();
				System.out.println("getFriendDetails Statement Executed !! ==> " + result.getStatement());
				results = processResultsAjax(result,correlationId);				
			}else {
				results = createBlankJSONResponse(correlationId);
			}

		}catch(Exception e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
			if(result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.print("getFriendDetails Result in Json Format ===> " + results);	
		return results;
	}
	
	private String processResults(ResultSet result,String correlationId) {
		JSONArray list = null;
		JSONObject obj = null;
		String results = null;
		String columnType = null;
		Map<String, String> MetadataMap = new HashMap<>();
		ArrayList<String> columnName = new ArrayList<>();
		
		try {
			ResultSetMetaData rsmd = result.getMetaData();
			for (int i = 1; i <= rsmd.getColumnCount();i++) {
				MetadataMap.put(rsmd.getColumnName(i),rsmd.getColumnTypeName(i));
				columnName.add(rsmd.getColumnName(i));
				System.out.println(rsmd.getColumnName(i) + " ==> " + rsmd.getColumnTypeName(i));
			}
			
			list = new JSONArray();
			obj=new JSONObject();
			while(result.next()) {
				obj=new JSONObject();
				for(String column : columnName) {
					columnType = MetadataMap.get(column);
					switch(columnType) {
					case "INT" :
					case "BIGINT" :
						System.out.println(column + " ==> " + result.getInt(column) );
						obj.put(column,result.getInt(column));
					break;
					
					case "VARCHAR" :
						System.out.println(column + " ==> " + result.getString(column) );
						obj.put(column,result.getString(column));
					break;
					
					case "DATE" :
						System.out.println(column + " ==> " + result.getDate(column) );
						obj.put(column,result.getDate(column).toString());
					break;
					
					case "CHAR" :
						System.out.println(column + " ==> " + result.getString(column) );
						obj.put(column,result.getString(column).toString());
					break;
					
					default :
						System.out.println(column + " ==> " + result.getObject(column).toString());
						obj.put(column,result.getObject(column));
					break;
					}
				}
				list.add(obj);
			}
			//Adding Resposne Code As Success
			//obj=new JSONObject();
			obj.put("code", 200);
			list.add(obj);

			//Passing request CorrelationId Back to response
			obj = AddResponseHeaders(list,correlationId);
			obj.put("code", 200);
			
			//Converting JSON to String for Sending the response.
			results = ConvertJSONToString(obj);
			
		} catch (SQLException e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
		}
		return results;
	}
	
	private String processResultsAjax(ResultSet result,String correlationId) {
		JSONArray list = null;
		JSONObject obj = null;
		String results = null;
		String columnType = null;
		Map<String, String> MetadataMap = new HashMap<>();
		ArrayList<String> columnName = new ArrayList<>();
		
		try {
			ResultSetMetaData rsmd = result.getMetaData();
			for (int i = 1; i <= rsmd.getColumnCount();i++) {
				MetadataMap.put(rsmd.getColumnName(i),rsmd.getColumnTypeName(i));
				columnName.add(rsmd.getColumnName(i));
				System.out.println(rsmd.getColumnName(i) + " ==> " + rsmd.getColumnTypeName(i));
			}
			
			list = new JSONArray();
			obj=new JSONObject();
			while(result.next()) {
				obj=new JSONObject();
				for(String column : columnName) {
					columnType = MetadataMap.get(column);
					switch(columnType) {
					case "INT" :
					case "BIGINT" :
						System.out.println(column + " ==> " + result.getInt(column) );
						obj.put(column,result.getInt(column));
					break;
					
					case "VARCHAR" :
						System.out.println(column + " ==> " + result.getString(column) );
						obj.put(column,result.getString(column));
					break;
					
					case "DATE" :
						System.out.println(column + " ==> " + result.getDate(column) );
						obj.put(column,result.getDate(column).toString());
					break;
					
					case "CHAR" :
						System.out.println(column + " ==> " + result.getString(column) );
						obj.put(column,result.getString(column).toString());
					break;
					
					default :
						//System.out.println(column + " ==> " + result.getObject(column).toString());
						//obj.put(column,result.getObject(column));
					break;
					}
				}
				list.add(obj);
			}
			//Adding Resposne Code As Success
			//obj=new JSONObject();
			//obj.put("code", 200);
			//list.add(obj);

			//Passing request CorrelationId Back to response
			obj = AddResponseHeaders(list,correlationId);
			obj.put("code", 200);
			
			//Converting JSON to String for Sending the response.
			results = ConvertJSONToString(obj);
			
		} catch (SQLException e) {
			e.printStackTrace();
			results = createBlankJSONResponse(correlationId);
		}finally {
		}
		return results;
	}
	
	private String createBlankJSONResponse(String correlationId) {
		JSONObject obj = new JSONObject();
		StringWriter out = new StringWriter();
		try {
			obj.put("correlationId", correlationId);
			obj.put("QueryResult", new JSONArray());
			obj.writeJSONString(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out.toString();
	}
	
	private JSONObject AddResponseHeaders(JSONArray list,String correlationId) {
		JSONObject obj = new JSONObject();
		try {
		//Passing request CorrelationId Back to response
		obj = new JSONObject();
		obj.put("correlationId", correlationId);
		obj.put("QueryResult", list);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	private String ConvertJSONToString(JSONObject obj) {
		String results = null;
		try {
			StringWriter out = new StringWriter();
			obj.writeJSONString(out);
			results = out.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}
}
